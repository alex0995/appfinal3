package com.example.appfinal3;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.appfinal3.accounts.AccountsFragment;
import com.example.appfinal3.categories.CategoriesFragment;
import com.example.appfinal3.reports.ReportsFragment;
import com.example.appfinal3.settings.SettingsFragment;
import com.example.appfinal3.transactions.TransactionsFragment;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> fragments = new ArrayList<>();
    //private PageFragment currentFragment;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments.clear();
        fragments.add(AccountsFragment.newInstance(0));
        fragments.add(CategoriesFragment.newInstance(1));
        fragments.add(TransactionsFragment.newInstance(2));
        fragments.add(ReportsFragment.newInstance(3));
        fragments.add(SettingsFragment.newInstance(4));
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    /*public PageFragment getCurrentFragment() {
        return currentFragment;
    }*/
}
