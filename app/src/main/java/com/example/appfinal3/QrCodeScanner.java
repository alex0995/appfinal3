package com.example.appfinal3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.ScanMode;
import com.google.zxing.BarcodeFormat;

import java.util.Collections;

public class QrCodeScanner extends AppCompatActivity {
    private CodeScanner QrCodeScanner;

    private static final int REQUEST_CODE_PERMISSION_CAMERA = 1512;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_scanner);

        int permissionStatus = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
            initScanQR();
        }
        else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA},
                    REQUEST_CODE_PERMISSION_CAMERA);
        }
    }

    public void initScanQR() {
        CodeScannerView scannerView = findViewById(R.id.scanner_view);

        QrCodeScanner = new CodeScanner(this, scannerView);
        QrCodeScanner.setFormats(Collections.singletonList(BarcodeFormat.QR_CODE)); //Сканировать только QR
        QrCodeScanner.setScanMode(ScanMode.SINGLE); //Сканировать 1 раз

        QrCodeScanner.setCamera(CodeScanner.CAMERA_BACK); //Задняя камера
        QrCodeScanner.setFlashEnabled(false); //Состояние вспышки по умолчанию
        QrCodeScanner.setAutoFocusEnabled(true); //Состояние автофокуса по умолчанию
        QrCodeScanner.setAutoFocusMode(AutoFocusMode.SAFE); //Автофокус с интервалом

        QrCodeScanner.setDecodeCallback(result -> runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(QrCodeScanner.this, result.getText(), Toast.LENGTH_SHORT).show();
            }
        }));

        scannerView.setOnClickListener(view -> QrCodeScanner.startPreview());
    }

    @Override
    protected void onResume() {
        super.onResume();
            if (QrCodeScanner != null) {
                QrCodeScanner.startPreview();
            }
    }

    @Override
    protected void onPause() {
        if (QrCodeScanner != null) {
            QrCodeScanner.releaseResources();
        }
        super.onPause();
    }
}