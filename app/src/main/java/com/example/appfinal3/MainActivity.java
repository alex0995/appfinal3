package com.example.appfinal3;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.github.mikephil.charting.data.Entry;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout toolbarLayout;
    private Toolbar toolbar;
   // private LineChart transactionChart;
    private AHBottomNavigationViewPager viewPager;
    private FloatingActionButton floatingButtonAdd;

    BottomSheetLayout bottomSheet;

    private BottomSheetBehavior bottomSheetBehavior;

    private AHBottomNavigation bottomNavigation;

    private final ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appBarLayout = findViewById(R.id.app_bar);
        toolbarLayout = findViewById(R.id.toolbar_layout);
        toolbar = findViewById(R.id.toolbar);

        viewPager = findViewById(R.id.view_pager);
        floatingButtonAdd = findViewById(R.id.floating_button_add);

        bottomSheet = findViewById(R.id.bottomsheet);
        /*bottomSheetBehavior = BottomSheetBehavior.from(accBottomSheet);
        // настройка состояний нижнего экрана
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        // настройка максимальной высоты
            //bottomSheetBehavior.setPeekHeight(0);
        // настройка возможности скрыть элемент при свайпе вниз
            bottomSheetBehavior.setHideable(false);
            //bottomSheetBehavior.isHideable();*/

        bottomNavigation = findViewById(R.id.bottom_navigation);

        setSupportActionBar(toolbar);
        toolbarLayout.setTitle(getTitle());

        appBarLayout.addOnOffsetChangedListener( new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float percentAlpha = 1;
                float percentVerticalOffset = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
                percentAlpha -= percentVerticalOffset;
                //transactionChart.setAlpha(percentAlpha);
                toolbarLayout.setTitle(" A" + percentAlpha + " V" + verticalOffset);
            }
        });

        /*transactionChart = findViewById(R.id.chart);
        LineDataSet lineDataSet = new LineDataSet(dataValues(), "Data set Line 1");
        lineDataSet.setColors(new int[] { R.color.red1, R.color.red2, R.color.red3, R.color.red4 }, this);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        LineData data = new LineData(dataSets);
        transactionChart.setData(data);
        transactionChart.setTouchEnabled(false);
        //chart.setBackgroundColor(R.color.red1);
        transactionChart.setDrawBorders(false);

        transactionChart.getAxisLeft().setEnabled(false);
        transactionChart.getAxisRight().setDrawAxisLine(false);
        transactionChart.getAxisRight().setDrawGridLines(false);
        transactionChart.getXAxis().setDrawAxisLine(false);
        transactionChart.getXAxis().setDrawGridLines(false);
        transactionChart.invalidate();*/

        floatingButtonAdd.setOnClickListener(view -> {
            Intent intent = new Intent(getBaseContext(), QrCodeScanner.class);
            startActivity(intent);
        });
        initUI();
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @NotNull
    private ArrayList<Entry> dataValues() {
        ArrayList<Entry> dataVal = new ArrayList<>();
        dataVal.add(new Entry(0, 15));
        dataVal.add(new Entry(1, 10));
        dataVal.add(new Entry(2, 12));
        dataVal.add(new Entry(3, 7));
        dataVal.add(new Entry(4, 11));

        return dataVal;
    }

    public void initBottomSheet() {
        //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        bottomSheet.showWithSheetView(LayoutInflater.from(this).inflate(R.layout.bottom_sheet, bottomSheet, false));
    }

    private void initUI() {
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.accounts_1, R.drawable.outline_credit_card_24, R.color.color_tab_1);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.category_2, R.drawable.outline_donut_small_24, R.color.color_tab_2);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.transactions_3, R.drawable.outline_receipt_24, R.color.color_tab_3);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.reports_4, R.drawable.outline_equalizer_24, R.color.color_tab_4);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.settings_5, R.drawable.outline_settings_24, R.color.color_tab_5);

        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);
        bottomNavigationItems.add(item3);
        bottomNavigationItems.add(item4);
        bottomNavigationItems.add(item5);

        bottomNavigation.addItems(bottomNavigationItems);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

        // Меняем цвет иконки активной вкладки
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW); //Изменение стиля заголовка
        bottomNavigation.setColored(false); //Изменение цвета панели при нажатии ктнопок
        bottomNavigation.setNotification ("3", 2); //Подключение уведомлений для значка

        bottomNavigation.setCurrentItem(0); //Фрагмент поумолчанию

        floatingButtonAdd.setVisibility(View.INVISIBLE);

        ViewGroup.LayoutParams layoutParamsAppBar = appBarLayout.getLayoutParams();
        final int appBarLayoutHeightDefault = layoutParamsAppBar.height;
        final int toolBarHeightDefault = toolbar.getLayoutParams().height;
        layoutParamsAppBar.height = toolBarHeightDefault;

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                viewPager.setCurrentItem(position, false); //Получить текущую позицию

                switch (position) {
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                        floatingButtonAdd.setVisibility(View.INVISIBLE);
                    break;
                    case 2:
                        floatingButtonAdd.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });
        viewPager.setOffscreenPageLimit(4); //Количество заранее подгруженных страниц
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager())); //Создать фрагменты

        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {

            }
        });
    }
}