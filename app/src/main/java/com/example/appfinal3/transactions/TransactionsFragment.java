package com.example.appfinal3.transactions;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appfinal3.R;
import com.example.appfinal3.SectionHeaderAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Source;
import com.h6ah4i.android.widget.advrecyclerview.composedadapter.ComposedAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TransactionsFragment extends Fragment {
    ComposedAdapter composedAdapter = new ComposedAdapter();
    //База данных FireStore
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build();

    private final CollectionReference transactionRef = db.collection("users").document("zmalex09@gmail.com")
            .collection("Transactions");

    Source source = Source.CACHE;

    private NoteAdapter noteAdapter;

    public TransactionsFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static TransactionsFragment newInstance(int index) {
        TransactionsFragment fragment = new TransactionsFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_recycler_view, container, false);
        initListTransaction(view);
        return view;
    }

    //Создать фрагмент списка транзакций
    private void initListTransaction(@NonNull View view) {
        final RecyclerView recyclerView;

        recyclerView = view.findViewById(R.id.rv_all);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        db.setFirestoreSettings(settings);

        Query query;
        FirestoreRecyclerOptions<Note> options;
        List<FirestoreRecyclerOptions<Note>> optionsArrayList = new ArrayList<>();
        List<NoteAdapter> noteAdapterList = new ArrayList<>();

        Date timestampStart = Calendar.getInstance().getTime();
        Date timestampEnd;

        for (int str = 31; str != 0; str--) {

            Calendar instanceStart = Calendar.getInstance();
            instanceStart.set(2020, Calendar.AUGUST, str);
            instanceStart.set(Calendar.HOUR, 0);
            instanceStart.set(Calendar.MINUTE, 0);
            instanceStart.set(Calendar.SECOND, 0);
            timestampStart = instanceStart.getTime();

            Calendar instanceEnd = Calendar.getInstance();
            instanceEnd.set(2020, Calendar.AUGUST, str);
            instanceEnd.set(Calendar.HOUR, 23);
            instanceEnd.set(Calendar.MINUTE, 59);
            instanceEnd.set(Calendar.SECOND, 59);
            timestampEnd = instanceEnd.getTime();

            query = transactionRef.orderBy("timestamp", Query.Direction.DESCENDING)
                    .startAt(timestampEnd).endAt(timestampStart);
            options = new FirestoreRecyclerOptions.Builder<Note>().setLifecycleOwner(this)
                      .setQuery(query, Note.class).build();

            noteAdapter = new NoteAdapter(options);

            noteAdapterList.add(noteAdapter);

            //optionsArrayList.add(options);
        }

        recyclerView.setAdapter(createComposedAdapter(noteAdapterList, timestampStart.toString()));


        noteAdapter.setOnItemClickListener((documentSnapshot, position) -> {
            String id = documentSnapshot.getId();
            //Timestamp timestamp = documentSnapshot.getTimestamp("timestamp");
            String path = documentSnapshot.getReference().getPath();
            Toast.makeText(getContext(), "Position: " + position + " ID: " + id +" ", Toast.LENGTH_SHORT).show();
        });
    }

    public ComposedAdapter createComposedAdapter(@NotNull List<NoteAdapter> noteAdapterList , String sectionTitle) {

        for (NoteAdapter noteAdapter: noteAdapterList) {

            int count = noteAdapter.getItemCount();
            if (count != 0) {
                composedAdapter.addAdapter(new SectionHeaderAdapter(sectionTitle));
            }            composedAdapter.addAdapter(noteAdapter);
        }
        return composedAdapter;
    }

    @Override
    public void onStart() {
        //Log.d("PAGING_LOG", "onStart of FrTransaction");
        super.onStart();
    }

    @Override
    public void onResume() {
        //Log.d("PAGING_LOG", "onResume of FrTransaction");
        super.onResume();
    }

    @Override
    public void onPause() {
        //Log.d("PAGING_LOG", "OnPause of FrTransaction");
        super.onPause();
    }

}