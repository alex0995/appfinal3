package com.example.appfinal3.transactions;

import java.util.Date;

public class Note {
    private double balance;
    private String account;
    private String category;
    private String description;
    private Date timestamp;


    public Note() {
        //Конструктор БД
    }

    public Note(String account, String category, String description, double balance, Date timestamp) {
        this.account = account;
        this.category = category;
        this.description = description;
        this.balance = balance;
        this.timestamp = timestamp;

    }

    public Date getTimestamp() {
        return timestamp;
    }

    public double getBalance() {
        return balance;
    }

    public String getAccount() {
        return account;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }
}
