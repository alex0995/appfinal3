package com.example.appfinal3.transactions;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appfinal3.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class NoteAdapter extends FirestoreRecyclerAdapter<Note, NoteAdapter.NoteHolder> {

    private int itemsSize;
    private OnItemClickListener listener;


    public NoteAdapter(@NonNull FirestoreRecyclerOptions<Note> options) {
        super(options);
    }

    @Override
    public void onDataChanged() {
         int in = getItemCount();
        itemsSize = getSnapshots().size();

        /*if (itemsSize > 0) {
            Log.d("PAGING_LOG", "Добавлена Транзакция");

        }*/

        // Вызывается каждый раз, когда создается новый снимок запроса. Вы можете использовать этот метод,
        // чтобы скрыть счетчик загрузки или проверить состояние «нет документов» и обновить свой пользовательский интерфейс.
        // ...
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull Note model) {
        holder.textViewCategory.setText(model.getCategory());
        holder.textViewAccount.setText(model.getAccount());
        holder.textViewBalance.setText(String.valueOf(model.getBalance()));
        holder.textViewDescription.setText(model.getDescription());
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false);
        return new NoteHolder(view);
    }

    class NoteHolder extends RecyclerView.ViewHolder {
        TextView textViewAccount;
        TextView textViewCategory;
        TextView textViewBalance;
        TextView textViewDescription;

        public NoteHolder(@NonNull View itemView) {
            super(itemView);
            textViewAccount = itemView.findViewById(R.id.textViewAccount);
            textViewBalance = itemView.findViewById(R.id.tvAccountBalance);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
