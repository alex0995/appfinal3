package com.example.appfinal3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SectionHeaderAdapter extends RecyclerView.Adapter<com.example.appfinal3.SectionHeaderAdapter.MyViewHolder> {
    static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewHead;

        public MyViewHolder(View itemView) {
            super(itemView);
            textViewHead = itemView.findViewById(R.id.tvHeadText);
        }
    }

    private String mSectionTitle;

    public SectionHeaderAdapter(String sectionTitle) {
        mSectionTitle = sectionTitle;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_section_header, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textViewHead.setText(mSectionTitle);
    }
}
