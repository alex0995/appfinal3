package com.example.appfinal3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appfinal3.accounts.account.Account;
import com.example.appfinal3.utils.ViewUtils;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class TestDnDAdapter extends FirestoreRecyclerAdapter<Account, TestDnDAdapter.AccountHolder>
        implements DraggableItemAdapter<TestDnDAdapter.AccountHolder>, View.OnClickListener {
    private final NumberFormat numberFormat = NumberFormat.getInstance();

    @Override
    public void onDataChanged() {
        // Вызывается каждый раз, когда создается новый снимок запроса. Вы можете использовать этот метод,
        // чтобы скрыть счетчик загрузки или проверить состояние «нет документов» и обновить свой пользовательский интерфейс.
        // ...
    }

    static class AccountHolder extends AbstractDraggableItemViewHolder {
        TextView tvAccountName;
        TextView tvAccountComment;
        TextView tvAccountBalance;
        View dragHandle;

        public AccountHolder(@NonNull View itemView) {
            super(itemView);
            tvAccountName = itemView.findViewById(R.id.tvAccountName);
            tvAccountComment = itemView.findViewById(R.id.tvAccountComment);
            tvAccountBalance = itemView.findViewById(R.id.tvAccountBalance);
            dragHandle = itemView.findViewById(R.id.container1);
        }
    }

    OnListItemClickMessageListener mOnItemClickListener;
    List<String> mItems = new ArrayList<>();;

    public TestDnDAdapter(@NonNull @NotNull FirestoreRecyclerOptions<Account> options, OnListItemClickMessageListener clickListener) {
        super(options);
        setHasStableIds(true);
        mOnItemClickListener = clickListener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected void onBindViewHolder(@NonNull AccountHolder holder, int position, @NonNull Account model) {
        holder.tvAccountName.setText(model.getName());
        holder.tvAccountComment.setText(model.getComment());

        numberFormat.setMaximumFractionDigits(2);
        if (model.getBalance()%1 != 0) {
            numberFormat.setMinimumFractionDigits(2);
        }
        else {
            numberFormat.setMinimumFractionDigits(0);
        }
        holder.tvAccountBalance.setText(numberFormat.format(model.getBalance()));
    }

    @NonNull
    @Override
    public AccountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_item, parent, false);
        AccountHolder ah = new AccountHolder(view);
        ah.dragHandle.setOnClickListener(this);
        return ah;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {

    }

    @Override
    public boolean onCheckCanStartDrag(@NonNull @NotNull TestDnDAdapter.AccountHolder holder, int position, int x, int y) {
        return ViewUtils.hitTest(holder.dragHandle, x, y);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(@NonNull @NotNull TestDnDAdapter.AccountHolder holder, int position) {
        return null;
    }

    @Override
    public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
        return true;
    }

    @Override
    public void onItemDragStarted(int position) {
        notifyDataSetChanged();
    }

    @Override
    public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        RecyclerView rv = RecyclerViewAdapterUtils.getParentRecyclerView(v);
        assert rv != null;
        RecyclerView.ViewHolder vh = rv.findContainingViewHolder(v);

        assert vh != null;
        int rootPosition = vh.getAdapterPosition();
        if (rootPosition == RecyclerView.NO_POSITION) {
            return;
        }
        // need to determine adapter local position like this:
        RecyclerView.Adapter rootAdapter = rv.getAdapter();
        assert rootAdapter != null;
        int localPosition = WrapperAdapterUtils.unwrapPosition(rootAdapter, this, rootPosition);

        String message = "CLICKED: Draggable item " + localPosition;

        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClicked(message);
        }
    }
}
