package com.example.appfinal3.accounts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.appfinal3.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class AccountsFragment extends Fragment {
    ViewPager2 viewPager2;

    public AccountsFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static AccountsFragment newInstance(int index) {
        AccountsFragment fragment = new AccountsFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_accounts, container, false);
        initListAccounts(view);
        return view;
    }

    private void initListAccounts(@NonNull View view) {
        viewPager2 = view.findViewById(R.id.viewPager2);
        viewPager2.setAdapter(new AccountsPagerAdapter(getActivity()));
        TabLayout tabLayout = view.findViewById(R.id.tabLayoutAccount);

        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText(R.string.tab_account);
                    break;
                case 1:
                    tab.setText(R.string.tab_budget);
                    break;
            }
        });
        tabLayoutMediator.attach();
    }
}