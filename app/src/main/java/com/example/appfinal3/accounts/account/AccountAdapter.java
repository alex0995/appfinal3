package com.example.appfinal3.accounts.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appfinal3.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

import java.text.NumberFormat;

public class AccountAdapter extends FirestoreRecyclerAdapter<Account, AccountAdapter.AccountHolder> {
    private AccountAdapter.OnItemClickListener listener;
    private final NumberFormat numberFormat = NumberFormat.getInstance();


    public AccountAdapter(@NonNull FirestoreRecyclerOptions<Account> options) {
        super(options);
    }

    @Override
    public void onDataChanged() {

        // Вызывается каждый раз, когда создается новый снимок запроса. Вы можете использовать этот метод,
        // чтобы скрыть счетчик загрузки или проверить состояние «нет документов» и обновить свой пользовательский интерфейс.
        // ...
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull AccountHolder holder, int position, @NonNull Account model) {
        holder.tvAccountName.setText(model.getName());
        holder.tvAccountComment.setText(model.getComment());

        numberFormat.setMaximumFractionDigits(2);
        if (model.getBalance()%1 != 0) {
            numberFormat.setMinimumFractionDigits(2);
        }
        else {
            numberFormat.setMinimumFractionDigits(0);
        }
        holder.tvAccountBalance.setText(numberFormat.format(model.getBalance()));
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    @NonNull
    @Override
    public AccountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_item, parent, false);
        return new AccountHolder(view);
    }

    class AccountHolder extends RecyclerView.ViewHolder {
        TextView tvAccountName;
        TextView tvAccountComment;
        TextView tvAccountBalance;

        public AccountHolder(@NonNull View itemView) {
            super(itemView);
            tvAccountName = itemView.findViewById(R.id.tvAccountName);
            tvAccountComment = itemView.findViewById(R.id.tvAccountComment);
            tvAccountBalance = itemView.findViewById(R.id.tvAccountBalance);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);

    }

    public void setOnItemClickListener(AccountAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
