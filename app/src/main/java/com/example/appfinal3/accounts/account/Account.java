package com.example.appfinal3.accounts.account;

public class Account {
    private String name;        //название счета
    private String comment;     //описание
    private String currency;    //валюта
    private double balance;     //баланс
    private Boolean archive;    //в архиве или нет

    public Account() {

    }   // Required empty public constructor

    public Account(String name, String comment, String currency, double balance, Boolean archive) {
        this.name = name;
        this.comment = comment;
        this.currency = currency;
        this.balance = balance;
        this.archive = archive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
