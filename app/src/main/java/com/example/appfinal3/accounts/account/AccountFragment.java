package com.example.appfinal3.accounts.account;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appfinal3.MyDraggableAdapter;
import com.example.appfinal3.OnListItemClickMessageListener;
import com.example.appfinal3.R;
import com.example.appfinal3.SectionHeaderAdapter;
import com.example.appfinal3.TestDnDAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.h6ah4i.android.widget.advrecyclerview.composedadapter.ComposedAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AccountFragment extends Fragment {
    //База данных FireStore
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true).build();
    private final CollectionReference transactionRef = db.collection("users").document("zmalex09@gmail.com")
            .collection("Accounts");

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_recycler_view, container, false);
        initAccount(view);
        return view;
    }

    public void initAccount(@NotNull View view) {
        Query query;
        FirestoreRecyclerOptions<Account> options;
        RecyclerView recyclerView = view.findViewById(R.id.rv_all);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        OnListItemClickMessageListener clickListener = message -> {
            View container = view.findViewById(R.id.container1);
            Snackbar.make(container, message, Snackbar.LENGTH_SHORT).show();
        };

        db.setFirestoreSettings(settings);

        query = transactionRef.whereEqualTo("archive", false);
        options = new FirestoreRecyclerOptions.Builder<Account>().setLifecycleOwner(this)
                .setQuery(query, Account.class).build();

        final DocumentReference docRef = db.collection("users")
                .document("zmalex09@gmail.com").collection("Accounts")
                .document("Счета - Алексей");

        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d("PAGING_LOG", "Listen failed.", e);
                    return;
                }
                //List<String, Object> mItems = new ArrayList<>();

                if (snapshot != null && snapshot.exists()) {
                    Map<String, Object> mItems = snapshot.getData();
                    Log.d("PAGING_LOG", "Current data: " + snapshot.getData());

                    Log.d("PAGING_LOG", "GET KEY: " + snapshot.toObject(Account.class));

                    snapshot.toObject(Account.class);
                } else {
                    Log.d("PAGING_LOG", "Current data: null");
                }
            }
        });

        RecyclerView.Adapter composedAdapter = createComposedAdapter(recyclerView, clickListener, options);

        recyclerView.setAdapter(composedAdapter);

        /*accountAdapter.setOnItemClickListener((documentSnapshot, position) -> {
            String id = documentSnapshot.getId();
            Toast.makeText(getContext(), "Position: " + position + " ID: " + id, Toast.LENGTH_SHORT).show();
            //((MainActivity)getActivity()).initBottomSheet();
        });*/
    }

    @NotNull
    private ComposedAdapter createComposedAdapter(RecyclerView rv, OnListItemClickMessageListener clickListener, FirestoreRecyclerOptions<Account> options) {
        RecyclerViewDragDropManager dragMgr = new RecyclerViewDragDropManager();
        dragMgr.setInitiateOnMove(false);
        dragMgr.setInitiateOnLongPress(true);
        dragMgr.setDraggingItemShadowDrawable((NinePatchDrawable) ContextCompat.getDrawable(requireActivity(), R.drawable.material_shadow_z3));

        ComposedAdapter composedAdapter = new ComposedAdapter();

        composedAdapter.addAdapter(new SectionHeaderAdapter("СЧЕТА"));
        composedAdapter.addAdapter(dragMgr.createWrappedAdapter(new TestDnDAdapter(options, clickListener)));


        dragMgr.attachRecyclerView(rv);

        return composedAdapter;
    }
}