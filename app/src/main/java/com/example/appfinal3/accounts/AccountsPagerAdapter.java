package com.example.appfinal3.accounts;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.appfinal3.accounts.account.AccountFragment;
import com.example.appfinal3.accounts.budget.BudgetFragment;

public class AccountsPagerAdapter extends FragmentStateAdapter {
    public AccountsPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new AccountFragment();
            default: return new BudgetFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
